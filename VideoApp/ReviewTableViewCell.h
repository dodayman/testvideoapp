//
//  ReviewTableViewCell.h
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *author;

@end
