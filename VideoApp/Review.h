//
//  Review.h
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Review : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *author;
@property(strong, nonatomic) NSString *content;
@property(strong, nonatomic) NSString *url;

@end
