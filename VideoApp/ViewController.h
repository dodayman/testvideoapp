//
//  ViewController.h
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoAppDataService.h"

@interface ViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, VideoAppDataServiceDelegate>

@property(weak, nonatomic) IBOutlet UICollectionView *videoCollectionView;

@end

