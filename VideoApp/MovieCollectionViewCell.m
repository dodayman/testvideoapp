//
//  MovieCollectionViewCell.m
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "MovieCollectionViewCell.h"

@interface MovieCollectionViewCell ()

@property(strong, nonatomic) NSCache *imageCache;
@property(strong, nonatomic) NSString *checkImageURL;

@end

@implementation MovieCollectionViewCell

- (void)loadImage:(NSURL*) url {
    
    self.checkImageURL = [url parameterString];
    
    UIImage *imageFromCache = [self.imageCache objectForKey:url];
    
    if (imageFromCache) {
        self.poster.image = imageFromCache;
        return;
    }
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error);
            return;
        } else {
            UIImage *image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.checkImageURL == [url parameterString]) {
                    self.poster.image = image;
                }
                [self.imageCache setObject:image forKey:url];
            });
        }
        
    }] resume];
}

@end
