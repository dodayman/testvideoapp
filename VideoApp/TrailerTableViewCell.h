//
//  TrailerTableViewCell.h
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrailerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIImageView *backdrop;
@property (weak, nonatomic) IBOutlet UILabel *overview;

- (void)playVideo: (NSString*)key;
- (void)loadBackdropWithURL: (NSURL*)url;

@end
