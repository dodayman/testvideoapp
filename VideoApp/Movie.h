//
//  Movie.h
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Trailer;

@interface Movie : NSObject

@property(assign, nonatomic) NSNumber *vote_count;
@property(strong, nonatomic) NSString *id;
@property(assign, nonatomic) Boolean video;
@property(assign, nonatomic) NSNumber *vote_average;
@property(strong, nonatomic) NSString *title;
@property(assign, nonatomic) NSNumber *popularity;
@property(strong, nonatomic) NSString *poster_path;
@property(strong, nonatomic) NSString *original_language;
@property(strong, nonatomic) NSString *original_title;
@property(strong, nonatomic) NSArray *genre_ids;
@property(strong, nonatomic) NSString *backdrop_path;
@property(assign, nonatomic) Boolean adult;
@property(strong, nonatomic) NSString *overview;
@property(strong, nonatomic) NSDate *release_date;

@property(strong, nonatomic) Trailer *trailer;
@property(strong, nonatomic) NSArray *images;
@property(strong, nonatomic) NSArray *reviews;

- (void)fetchReviews;
- (void)fetchTrailer;
- (void)loadBackdrop;

@end
