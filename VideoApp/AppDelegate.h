//
//  AppDelegate.h
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

