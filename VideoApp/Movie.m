//
//  Movie.m
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "Movie.h"
#import "Trailer.h"
#import "Review.h"
#import "VideoAppDataService.h"

@implementation Movie

////////////////////////////////////////////////////////////////////////////

- (void)fetchReviews {
    
    NSMutableArray *parsedReviews = [[NSMutableArray alloc] init];;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/%@/reviews?api_key=%@&language=en-US&page=1", self.id, [VideoAppDataService shared].API_KEY]];
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error);
            return;
        } else {
            @try {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                if ([dict objectForKey:@"total_results"] == 0) {
                    
                } else {
                    NSArray *results = [dict objectForKey:@"results"];
                    
                    for (NSDictionary *review in results) {
                        Review *newReview = [[Review alloc] init];
                        [newReview setValuesForKeysWithDictionary:review];
                        [parsedReviews addObject:newReview];
                    }
                    
                    self.reviews = parsedReviews;
                }
            }
            @catch (NSException *exception){
                NSLog(@"exception - %@", exception.reason);
            }
        }
        
    }] resume];
    
}

////////////////////////////////////////////////////////////////////////////

- (void)fetchTrailer {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/%@/videos?api_key=%@&language=en-US", self.id, [VideoAppDataService shared].API_KEY]];
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error);
            return;
        } else {
            @try {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSArray *results = [dict objectForKey:@"results"];
                if (results.count > 0) {
                    NSDictionary *trailerData = results.firstObject;
                    self.trailer = [[Trailer alloc] init];
                    self.trailer.id = [trailerData objectForKey:@"id"];
                    self.trailer.name = [trailerData objectForKey:@"name"];
                    self.trailer.key = [trailerData objectForKey:@"key"];
                    self.trailer.site = [trailerData objectForKey:@"site"];
                    
                }
                NSLog(@"exception - %@", self.trailer);
            }
            @catch (NSException *exception){
                NSLog(@"exception - %@", exception.reason);
            }
        }
        
    }] resume];
}


@end
