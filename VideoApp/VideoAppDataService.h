//
//  VideoAppDataService.h
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VideoAppDataServiceDelegate <NSObject>
@required

- (void)reloadData;

@end

@interface VideoAppDataService : NSObject

@property(weak, nonatomic) id<VideoAppDataServiceDelegate> delegate;
@property(strong, nonatomic) NSMutableArray *videos;
@property(strong, nonatomic) NSMutableArray *reviews;
@property(strong, nonatomic, readonly) NSString *API_KEY;

+ (instancetype)shared;

- (void)fetchUpcomingVideos;
- (NSURL*)makeImageURLWithPath:(NSString*)imagePath andQuality:(NSString*)quality;

@end
