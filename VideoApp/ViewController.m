//
//  ViewController.m
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "ViewController.h"
#import "MovieCollectionViewCell.h"
#import "VideoAppDataService.h"
#import "Movie.h"
#import "DetailVideoViewController.h"

@interface ViewController ()

@property(strong, nonatomic) NSMutableArray *videos;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [VideoAppDataService shared].delegate = self;
}


- (void)setup {
    self.navigationItem.title = @"Home";
    [self.videoCollectionView registerNib:[UINib nibWithNibName:@"MovieCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MovieCell"];
}

////////////////////////////////////////////////////////////////////////////

- (void)reloadData {
    [self.videoCollectionView reloadData];
}


////////////////////////////////////////////////////////////////////////////
// CollectionView


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [VideoAppDataService shared].videos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MovieCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MovieCell" forIndexPath:indexPath];
    
    Movie *video = [VideoAppDataService shared].videos[indexPath.item];
    
    NSURL *url = [[VideoAppDataService shared] makeImageURLWithPath:video.poster_path andQuality:@"w320"];
    
    cell.title.text = video.title;
    [cell loadImage:url];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = self.view.bounds.size.width/2 - 20;
    CGFloat height = width/0.66 + 35.f;
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(20, 10, 20, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Movie *video = [VideoAppDataService shared].videos[indexPath.item];
    [video fetchReviews];
    [video fetchTrailer];
    [self performSegueWithIdentifier:@"MovieDetail" sender:video];
}


////////////////////////////////////////////////////////////////////////////

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"MovieDetail"]) {
        DetailVideoViewController *vc = [segue destinationViewController];
        vc.movie = sender;
    }
}

@end
