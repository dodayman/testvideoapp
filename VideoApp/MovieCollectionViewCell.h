//
//  MovieCollectionViewCell.h
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *poster;
@property (weak, nonatomic) IBOutlet UILabel *title;

-(void)loadImage:(NSURL*) url;

@end
