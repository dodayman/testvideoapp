//
//  DetailVideoViewController.m
//  VideoApp
//
//  Created by Ivan Smirnov on 11/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "DetailVideoViewController.h"
#import "Movie.h"
#import "Trailer.h"
#import "Review.h"
#import "TrailerTableViewCell.h"
#import "ReviewTableViewCell.h"
#import "VideoAppDataService.h"
#import <AVFoundation/AVFoundation.h>

@interface DetailVideoViewController ()

@end

@implementation DetailVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}


- (void)createDescription {
    UILabel *description = [[UILabel alloc] init];
    description.text = self.description;
}


////////////////////////////////////////////////////////////////////////////
// TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int sections = 1;
    if (self.movie.reviews.count > 0) {
        sections += 1;
    } 
    return sections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return self.movie.reviews.count;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [label.font fontWithSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    
    if (section == 1) {
        label.text = @"Reviews";
    }
    
    return label;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        TrailerTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TrailerCell"];
        
        if (self.movie.trailer) {
            CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width * 9 / 16);
            cell.webview.frame = rect;
            if ([self.movie.trailer.site  isEqual: @"YouTube"]) {
                [cell playVideo:self.movie.trailer.key];
            }
            cell.overview.text = self.movie.overview;
        } else {
            NSURL *url = [[VideoAppDataService shared] makeImageURLWithPath: self.movie.backdrop_path andQuality:@"w320"];
            [cell loadBackdropWithURL:url];
        }
        
        return cell;
        
    } else if (indexPath.section == 1) {
        
        Review *review = self.movie.reviews[indexPath.row];
        ReviewTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ReviewCell"];
        cell.content.text = review.content;
        cell.author.layer.opacity = 0.4;
        cell.author.text = review.author;
        
        return cell;
        
    } else {
        return [[UITableViewCell alloc]init];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    } else {
        return 30;
    }
    
}

@end
