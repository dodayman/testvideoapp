//
//  Trailer.h
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trailer : NSObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *key;
@property(strong, nonatomic) NSString *site;

@end
