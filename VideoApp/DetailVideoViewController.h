//
//  DetailVideoViewController.h
//  VideoApp
//
//  Created by Ivan Smirnov on 11/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface DetailVideoViewController : UITableViewController

@property(strong, nonatomic) Movie *movie;


@end
