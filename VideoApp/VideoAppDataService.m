//
//  VideoAppDataService.m
//  VideoApp
//
//  Created by Ivan Smirnov on 10/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "VideoAppDataService.h"
#import "Movie.h"

@interface VideoAppDataService ()

@property(strong, nonatomic) NSString *baseURL;
@property(strong, nonatomic) NSString *imageURL;
@property(strong, nonatomic, readwrite) NSString *API_KEY;

@end

@implementation VideoAppDataService

+ (id)shared {
    static VideoAppDataService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

////////////////////////////////////////////////////////////////////////////

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.API_KEY = @"eb2f99008f02df384b35450d418f494f";
        self.imageURL = @"http://image.tmdb.org/t/p/";
        self.videos = [[NSMutableArray alloc] init];
        [self fetchUpcomingVideos];
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////

- (void)fetchUpcomingVideos {
    
    NSMutableArray *parsedVideos = [[NSMutableArray alloc] init];;
    
    NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *url = [NSString stringWithFormat: @"https://api.themoviedb.org/3/movie/upcoming?language=en-US&api_key=%@", self.API_KEY];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod: @"GET"];
    [request setHTTPBody: postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        @try {
                                                            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                            NSArray *results = [dict objectForKey:@"results"];
                                                            
                                                            for (NSDictionary *video in results) {
                                                                Movie *newVideo = [[Movie alloc] init];
                                                                [newVideo setValuesForKeysWithDictionary:video];
                                                                [parsedVideos addObject:newVideo];
                                                            }
                                                            
                                                            if ([parsedVideos count] > 0) {
                                                                [self.videos addObjectsFromArray:parsedVideos];
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [self.delegate reloadData];
                                                            });
                                                            
                                                        }
                                                        @catch (NSException *exception){
                                                            NSLog(@"%@", exception.reason);
                                                        }
                                                    }
                                                }];
    [dataTask resume];
    
}

////////////////////////////////////////////////////////////////////////////

- (NSURL*)makeImageURLWithPath:(NSString*)imagePath andQuality:(NSString*)quality {
    return [[NSURL alloc] initWithString: [NSString stringWithFormat:@"%@%@%@", self.imageURL, quality, imagePath]];
}

@end
