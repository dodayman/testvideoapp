//
//  TrailerTableViewCell.m
//  VideoApp
//
//  Created by Ivan Smirnov on 12/09/2017.
//  Copyright © 2017 Doday. All rights reserved.
//

#import "TrailerTableViewCell.h"
#import "VideoAppDataService.h"

@implementation TrailerTableViewCell


- (void)playVideo: (NSString*)key {
    NSString *youtubeURL = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@", key];;
    
    self.webview.backgroundColor = [UIColor clearColor];
    self.webview.opaque = NO;
    
    
    NSString *videoHTML = [NSString stringWithFormat:@"\
                           <html>\
                           <head>\
                           <style type=\"text/css\">\
                           iframe {position:absolute; top:0;}\
                           body {background-color:#000; margin:0;}\
                           </style>\
                           </head>\
                           <body>\
                           <iframe width=\"100%%\" height=\"100%%\" src=\"%@?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe>\
                           </body>\
                           </html>", youtubeURL];
    
    [self.webview loadHTMLString:videoHTML baseURL:nil];
}

////////////////////////////////////////////////////////////////////////////

- (void)loadBackdropWithURL: (NSURL*)url {
    
    if (url) {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (error) {
                NSLog(@"%@", error);
                return;
            } else {
                UIImage *loadedImage = [UIImage imageWithData:data];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.webview.hidden = true;
                    CGRect rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.width / 0.66);
                    self.backdrop.frame = rect;
                    self.backdrop.image = loadedImage;
                });
            }
            
        }] resume];
    }
    
}

@end
